export class ReservationTime {
    private id: number;
    private time: string;


	constructor($id: number, $time: string) {
		this.id = $id;
		this.time = $time;
	}


    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $time
     * @return {string}
     */
	public get $time(): string {
		return this.time;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $time
     * @param {string} value
     */
	public set $time(value: string) {
		this.time = value;
	}

}