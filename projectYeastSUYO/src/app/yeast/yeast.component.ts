import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

import { Yeast } from '../model/yeast';
import { Taxonomy } from '../model/taxonomy';

@Component({
  selector: 'app-yeast',
  templateUrl: './yeast.component.html',
  styleUrls: ['./yeast.component.css'],
  providers: [DatePipe]
})
export class YeastComponent implements OnInit {

  //properties
  //objYeast: Yeast[]=[];
  objYeast: Yeast;
  arrayTaxonomy: Taxonomy[] = [];

  constructor(private datePipe: DatePipe) { }

  ngOnInit() {
    this.objYeast = new Yeast();
    this.objYeast.classificationDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.createTaxonomyClases();
    this.createTaxonomyDomain();

  }



  yeastEntry(): void {
    console.log(this.objYeast);
  }

  createTaxonomyDomain(): void {
    let arrayTaxonomyAux: string[] = ["Eukarya", "Prokaryota"];
    let tableTaxonomy: Taxonomy; //Temp variable

    for (let i: number = 0; i < arrayTaxonomyAux.length; i++) {
      tableTaxonomy = new Taxonomy(arrayTaxonomyAux[i]);
      this.arrayTaxonomy.push(tableTaxonomy);
    }
    console.log(this.arrayTaxonomy);
  }

  createTaxonomyClases(): void {

    // let clasesYeast: string[] = ["Ascomycota", "Basidiomycota"];

    // for (let i: number = 0; i < clasesYeast.length; i++) {
    //   this.arrayTaxonomy.push(new Taxonomy(clasesYeast[i]));
    // }

  }

}
