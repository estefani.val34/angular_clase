import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { YeastComponent } from './yeast/yeast.component';
import { EnvironmentComponent } from './environment/environment.component';
import { PatologyComponent } from './patology/patology.component';
import { GenesComponent } from './genes/genes.component';
import { ProductsComponent } from './products/products.component';
import { MetabolismComponent } from './metabolism/metabolism.component';
import { AppMinLengthDirective } from './app-min-length.directive';


const appRoutes: Routes = [
  { path: 'app-yeast', component: YeastComponent},
  { path: 'app-products', component: ProductsComponent},
  { path: 'app-genes', component: GenesComponent},
  { path: 'app-patology', component: PatologyComponent},
  { path: 'app-metabolism', component: MetabolismComponent},
  { path: 'app-environment', component: EnvironmentComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    YeastComponent,
    EnvironmentComponent,
    PatologyComponent,
    GenesComponent,
    ProductsComponent,
    MetabolismComponent,
    AppMinLengthDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    RouterModule.forRoot(
      appRoutes, {enableTracing: false}  //True for debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
