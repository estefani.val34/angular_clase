import { Component, OnInit } from '@angular/core';
import { Environment } from '../model/environment';

@Component({
  selector: 'app-environment',
  templateUrl: './environment.component.html',
  styleUrls: ['./environment.component.css']
})
export class EnvironmentComponent implements OnInit {
//properties
  objEnvironment: Environment;
  
  


  constructor() { }

  ngOnInit() {
    this.objEnvironment = new Environment();
  }

  getEnvironments(){
    console.log(this.objEnvironment)
  }
}
