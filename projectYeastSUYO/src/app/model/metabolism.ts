export class Metabolism{
    private _aerobic: Boolean;
    private _temperature: string;//ngOnInit aparece unidades temperatura
    private _nutrition: string;

	constructor(aerobic?: Boolean, temperature?: string, nutrition?: string) {
		this._aerobic = aerobic;
		this._temperature = temperature;
		this._nutrition = nutrition;
	}

    /**
     * Getter aerobic
     * @return {Boolean}
     */
	public get aerobic(): Boolean {
		return this._aerobic;
	}

    /**
     * Getter temperature
     * @return {string}
     */
	public get temperature(): string {
		return this._temperature;
	}

    /**
     * Getter nutrition
     * @return {string}
     */
	public get nutrition(): string {
		return this._nutrition;
	}

    /**
     * Setter aerobic
     * @param {Boolean} value
     */
	public set aerobic(value: Boolean) {
		this._aerobic = value;
	}

    /**
     * Setter temperature
     * @param {string} value
     */
	public set temperature(value: string) {
		this._temperature = value;
	}

    /**
     * Setter nutrition
     * @param {string} value
     */
	public set nutrition(value: string) {
		this._nutrition = value;
	}

}