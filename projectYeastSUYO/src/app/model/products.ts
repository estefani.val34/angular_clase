export class Products{
    private _name: string;    
    private _fermentation: string;
    private _image: string;//path a la imagen

	constructor(name?: string, fermentation?: string, image?: string) {
		this._name = name;
		this._fermentation = fermentation;
		this._image = image;
	}

    /**
     * Getter name
     * @return {string}
     */
	public get name(): string {
		return this._name;
	}

    /**
     * Getter fermentation
     * @return {string}
     */
	public get fermentation(): string {
		return this._fermentation;
	}

    /**
     * Getter image
     * @return {string}
     */
	public get image(): string {
		return this._image;
	}

    /**
     * Setter name
     * @param {string} value
     */
	public set name(value: string) {
		this._name = value;
	}

    /**
     * Setter fermentation
     * @param {string} value
     */
	public set fermentation(value: string) {
		this._fermentation = value;
	}

    /**
     * Setter image
     * @param {string} value
     */
	public set image(value: string) {
		this._image = value;
	}

}