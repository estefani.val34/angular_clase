import { Component, OnInit } from '@angular/core';
import { Patology } from '../model/patology';

@Component({
  selector: 'app-patology',
  templateUrl: './patology.component.html',
  styleUrls: ['./patology.component.css']
})
export class PatologyComponent implements OnInit {

    //properties
    objPatology: Patology;
  constructor() { }

  ngOnInit() {
    this.objPatology= new Patology();
  }
  patologyEntry(): void{
    console.log(this.objPatology);

  }
}
