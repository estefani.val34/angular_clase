import { Component, OnInit } from '@angular/core';
import { Products } from '../model/products';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  //properties
  objProducts: Products;
  constructor() { }

  ngOnInit() {
    this.objProducts= new Products();
  }
  getProducts(){
    console.log(this.objProducts);
  }
}
