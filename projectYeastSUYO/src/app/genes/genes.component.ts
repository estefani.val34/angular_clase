import { Component, OnInit } from '@angular/core';
import { Genes } from '../model/genes';

@Component({
  selector: 'app-genes',
  templateUrl: './genes.component.html',
  styleUrls: ['./genes.component.css']
})
export class GenesComponent implements OnInit {

  //properties
  objGenes: Genes;
  
  constructor() { }

  ngOnInit() {
    this.objGenes = new Genes();
  }

  getGenes(){
    console.log(this.objGenes);
  }
}
