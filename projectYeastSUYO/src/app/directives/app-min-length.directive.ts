import { Directive } from '@angular/core';

import { Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appAppMinLength]'
})
export class AppMinLengthDirective implements Validator{

  constructor() { }
  validate(formFieldValidate: AbstractControl):
  {[key: string]: any}
  {
  let validInput: boolean = false;
    if(formFieldValidate && formFieldValidate.value && formFieldValidate.value.length > 6){
      validInput = true;
    }
    return validInput?null:{'isNotCorrect':true};
    
  }


}
