export class Patology{
    private _hpatologenic: boolean;
    private _symptoms: string;
    private _lethal: boolean;

	constructor(hpatologenic?: boolean, symptoms?: string, lethal?: boolean) {
		this._hpatologenic = hpatologenic;
		this._symptoms = symptoms;
		this._lethal = lethal;
	}

    /**
     * Getter hpatologenic
     * @return {boolean}
     */
	public get hpatologenic(): boolean {
		return this._hpatologenic;
	}

    /**
     * Getter symptoms
     * @return {string}
     */
	public get symptoms(): string {
		return this._symptoms;
	}

    /**
     * Getter lethal
     * @return {boolean}
     */
	public get lethal(): boolean {
		return this._lethal;
	}

    /**
     * Setter hpatologenic
     * @param {boolean} value
     */
	public set hpatologenic(value: boolean) {
		this._hpatologenic = value;
	}

    /**
     * Setter symptoms
     * @param {string} value
     */
	public set symptoms(value: string) {
		this._symptoms = value;
	}

    /**
     * Setter lethal
     * @param {boolean} value
     */
	public set lethal(value: boolean) {
		this._lethal = value;
	}

}