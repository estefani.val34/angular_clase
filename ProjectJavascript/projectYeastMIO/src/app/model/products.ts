import { Fermentation } from './fermentation';

export class Products{
    private _name: string;    
    private _fermentation: Fermentation; //select
    private _image: string;//path a la imagen

    /**
     * 
     * @param name 
     * @param fermentation 
     * @param image 
     */
	constructor(name?: string, fermentation?: Fermentation, image?: string) {
		this._name = name;
		this._fermentation = fermentation;
		this._image = image;
	}

    /**
     * Getter name
     * @return {string}
     */
	public get name(): string {
		return this._name;
	}

    /**
     * Getter fermentation
     * @return {Fermentation}
     */
	public get fermentation(): Fermentation {
		return this._fermentation;
	}

    /**
     * Getter image
     * @return {string}
     */
	public get image(): string {
		return this._image;
	}

    /**
     * Setter name
     * @param {string} value
     */
	public set name(value: string) {
		this._name = value;
	}

    /**
     * Setter fermentation
     * @param {Fermentation} value
     */
	public set fermentation(value: Fermentation) {
		this._fermentation = value;
	}

    /**
     * Setter image
     * @param {string} value
     */
	public set image(value: string) {
		this._image = value;
	}

	
}