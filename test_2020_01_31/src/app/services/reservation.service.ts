import { Injectable } from '@angular/core';

import { ReservationTime } from '../model/ReservationTime';
import { TablePreference } from '../model/TablePreference';
import { SpecialRequests } from '../model/SpecialRequests';

@Injectable({
  providedIn: 'root' // SE PUEDE USAR EN TODO EL PROYECTO 
})
export class ReservationService {

  constructor() { }


  createReservationTimes() :ReservationTime[]{
    let reservationTimes:ReservationTime[]=[];

    let reservationTimesAux: string[] = ["12:00", "13:00", "14:00", "15:00"];

    for (let i: number = 0; i < reservationTimesAux.length; i++) {
      reservationTimes.push(new ReservationTime(i, reservationTimesAux[i]));
    }

    return reservationTimes;
  }
  createReservationPreferences() : TablePreference[]{
    let tablePreferences:TablePreference[]=[];
    let tablePreferencesAux: string[] =
      ["Next to the window", "Next to the door",
        "Private room"];
    let tablePreference: TablePreference; //Temp variable

    for (let i: number = 0; i < tablePreferencesAux.length; i++) {
      tablePreference = new TablePreference(i,
        tablePreferencesAux[i], i * 2 + 3);
      tablePreferences.push(tablePreference);
    }
    return (tablePreferences);
  }

  createSpecialRequests():SpecialRequests[] {
    let specialRequests: SpecialRequests[]=[];
    let specialRequestAux: string[] =
      ["Vegetarian menu", "Lactose intolerance",
        "Celiac"];
    let specialRequest: SpecialRequests; //Temp variable

    for (let i: number = 0; i < specialRequestAux.length; i++) {
      specialRequest = new SpecialRequests(i,
        specialRequestAux[i], i * 3 + 1);
      specialRequests.push(specialRequest);
    }
    return (specialRequests);

  }
}
