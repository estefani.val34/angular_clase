import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';

registerLocaleData(localeES);

import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

import { ReservationEntryComponent } from './reservation-entry/reservation-entry.component';
import { ReservationManagementComponent } from './reservation-management/reservation-management.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InputValidationDirective } from './directives/input-validation.directive';

import {CookieService} from 'ngx-cookie-service';

import {NgxPaginationModule} from 'ngx-pagination';

const appRoutes: Routes = [
  { path: 'new-reservation',  component: ReservationEntryComponent},
  { path: 'reservation-management',  component: ReservationManagementComponent},
  { path: '',  redirectTo: '/new-reservation', pathMatch: 'full' },
  { path: '**',  component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ReservationEntryComponent,
    ReservationManagementComponent,
    PageNotFoundComponent,
    InputValidationDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    //AppRoutingModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false} //Debugging console disabled
    ),
    NgxPaginationModule
  ],
  providers: [ CookieService,
    {provide: LOCALE_ID,
    useValue: 'es'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
