export class TablePreference{
    private id: number;
    private preference: string;
    private price: number;


	constructor($id: number, $preference: string, $price: number) {
		this.id = $id;
		this.preference = $preference;
		this.price = $price;
	}


    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $preference
     * @return {string}
     */
	public get $preference(): string {
		return this.preference;
	}

    /**
     * Getter $price
     * @return {number}
     */
	public get $price(): number {
		return this.price;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $preference
     * @param {string} value
     */
	public set $preference(value: string) {
		this.preference = value;
	}

    /**
     * Setter $price
     * @param {number} value
     */
	public set $price(value: number) {
		this.price = value;
	}

}