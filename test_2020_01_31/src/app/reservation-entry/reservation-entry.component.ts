
import { Component, OnInit, ViewChild } from '@angular/core';//ViewChild acceder al dom
import { DatePipe } from '@angular/common';

import { Reservation } from '../model/Reservation';
import { ReservationTime } from '../model/ReservationTime';
import { TablePreference } from '../model/TablePreference';
import { SpecialRequests } from '../model/SpecialRequests';
import { CookieService } from 'ngx-cookie-service';

import { ReservationService } from '../services/reservation.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-reservation-entry',
  templateUrl: './reservation-entry.component.html',
  styleUrls: ['./reservation-entry.component.css'],
  providers: [DatePipe]
})
export class ReservationEntryComponent implements OnInit {
  //Properties
  reservation: Reservation;
  recommendations: String[][] = [["Restaurants", "Rates"], ["El celler", "***"], ["Diverxo", "**"]];
  reservationTimes: ReservationTime[] = [];
  tablePreferences: TablePreference[] = [];
  specialRequests: SpecialRequests[] = [];
  cookieObj: any;

  //solo hacemos de html a ts. 
  @ViewChild('reservationEntryForm', null) // null es una serie de opciones, si no le pones nada te coge las opciones por defecto 
  reservationEntryForm: HTMLFormElement;
  // variable de htlm del form , null otras opciones . acceder desde el ts al html. Solo hacer en casos concretos 
  @ViewChild('divCheckBox', null) // Recoger ELEMENTOS DE HTML 
  divCheckBox: HTMLFormElement; //tipo de la declaracion 

  constructor(
    private cookieService: CookieService,//estamos recibiendo un parametro y se convierte en un atributo que se puede usar en toda la clase 
    private datePipe: DatePipe,
    private reservationService: ReservationService) { }

  ngOnInit() {

    this.reservationTimes = this.reservationService.createReservationTimes();

    this.tablePreferences = this.reservationService.createReservationPreferences();

    this.specialRequests = this.reservationService.createSpecialRequests();

    this.inicializeFOrm();

    this.getCookie();


  }

  ngAfterViewInit() { // LLAMA A ESTO UNA VEZ LA PAGINA ESTA CARGADa, metodo ocurre cuanod esta todo cargado
    this.getCookieCHeckBox();
  }

  getCookieCHeckBox() {
    if (this.cookieObj) {
      //bucle con todas la cookies del checkbox, y chekearlar a true 
     for(let spAux of this.cookieObj._specialRequests){ // este esta marcado ponerl true 
      this.divCheckBox.nativeElement.children[spAux.id].children[0].checked=true;
      console.log(this.divCheckBox.nativeElement.children[spAux.id]);
     }
    }
  }


  /* reset(){
     this.reservationEntryForm.reset();
     this.inicializeFOrm();
   }*/


  inicializeFOrm() {
    this.reservationEntryForm.reset();// resetear todos los campos del formulario, DOM , todos los campos del formulario se borran 
    //this.reservationEntryForm.childNodes.item();//coleccion de nodos del DOM 

    // para saber en que estado esta el campo, 
    this.reservationEntryForm.form.markAsPristine(); //te los marca como pristine , te salta cuando has entrado el texto, nunca has entrado ni salido 
    this.reservationEntryForm.form.markAsUntouched();//has entrado, lo has tocado, lo has borrado y salido 

    this.reservation = new Reservation();

    this.reservation.reservationDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.reservation.reservationTime = this.reservationTimes[2];
    this.reservation.tablePreference = this.tablePreferences[0];
    this.reservation.specialRequests = [];
    this.reservation.totalPrice = 18;

  }

  getCookie() {
    //comprovar si existe la cookie, hare cosas
    if (this.cookieService.check("reservation")) {
       this.cookieObj  = JSON.parse(this.cookieService.get("reservation")); // la obtengo 
      //jason parse-> string a objeto ; stringly: de obj a s
      Object.assign(this.reservation, this.cookieObj); //se me vuelven a rellenar los campos, aunque no lo hace muy bien , no coge los objetos requeridos por el usuario request --> hacerlo a mano

      this.reservation.reservationTime =
        this.reservationTimes[this.cookieObj._reservationTime.id];

      this.reservation.tablePreference =
        this.tablePreferences[this.cookieObj._tablePreference.id];

      this.reservation.specialRequests = [] // coger el check box, no sale
      for (let spAux of this.cookieObj._specialRequests) {
        // this.reservation.specialRequests.push(
        // this.specialRequests[spAux.id])//lo coge del array
        this.addRemoveSpecialRequest(spAux);
      }
      /**
     * Copy the values of all of the enumerable own properties from one or more source objects to a
     * target object. Returns the target object.
     * @param target The target object to copy to.
     * @param source The source object from which to copy properties.
     */
    }


  }

  reservationEntry(): void {
    this.cookieService.set("reservation", JSON.stringify(this.reservation));//crear cookie
    console.log(this.reservation);
  }



  calculateTotalPrice(): void {

    this.reservation.totalPrice = 15;

    //for specialRequests
    for (let specialRequest of this.reservation.specialRequests) {
      // console.log("specialRequest");
      // console.log(specialRequest);
      this.reservation.totalPrice = this.reservation.totalPrice + specialRequest.$price;
    }

    //Add tablePreference
    this.reservation.totalPrice =
      this.reservation.totalPrice +
      this.reservation.tablePreference.$price;
    // console.log("tablePreference");
    // console.log(this.reservation.tablePreference);

  }

  addRemoveSpecialRequest(specialRequest: SpecialRequests): void {
    //If indexOf returns -1 means the object wasn't found
    //So it has to be added
    if (this.reservation.specialRequests
      .indexOf(specialRequest) == -1) {
      this.reservation.specialRequests.push(specialRequest);
      //Otherwise (indexOf returns != -1), the object was found and has to be removed
    } else {
      this.reservation.specialRequests.splice
        (this.reservation.specialRequests.
          indexOf(specialRequest), 1);
    }
    this.calculateTotalPrice();
  }
}




