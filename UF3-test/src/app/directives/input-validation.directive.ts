import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[inputMinLength]',//es el nombre de el atributo que voy a poner a mi componente
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: InputValidationDirective,
    multi: true
  }]

}) // cada directiva solo puede tener un metodo validate ; para cada metodo que hay que validar se ha de crear una directive 
export class InputValidationDirective implements Validator {

  constructor() { }

  /**
    * @description
    * Method that performs synchronous validation against the provided control.
    *
    * @param control The control to validate against.
    *
    * @returns A map of validation errors if validation fails,
    * otherwise null.
    */
  validate(formFieldToValidate: AbstractControl): { [key: string]: any } {// trnga un minimo de 6 caracteres
    let validInput: boolean = false;
    if (formFieldToValidate && formFieldToValidate.value && formFieldToValidate.value.length > 6) {//formFieldToValidate !=null
      validInput = true;
    }
    return validInput? null : {'isNotCorrect':true};//{'isNotCorrect':true} al caltogo de errores le añado este error 
  }

}
