import { ReservationTime } from '../model/reservationTime'
import { TablePreference } from './tablePreference';
import { SpecialRequests } from './specialRequests';

export class Reservation{
    //Properties
    private _id: number;
    private _name: String;
    private _surname: String;
    private _email: String;
    private _phone: String;
    private _reservationDate: Date;
    private _reservationTime: ReservationTime;
    private _tablePreference: TablePreference;
    private _specialRequests: SpecialRequests[];
    private _totalPrice: number;

	constructor(id?: number, name?: String, surname?: String, email?: String, phone?: String, reservationDate?: Date, reservationTime?: ReservationTime, tablePreference?: TablePreference, specialRequests?: SpecialRequests[], totalPrice?:number) {
		this._id = id;
		this._name = name;
		this._surname = surname;
		this._email = email;
		this._phone = phone;
		this._reservationDate = reservationDate;
		this._reservationTime = reservationTime;
		this._tablePreference = tablePreference;
        this._specialRequests = specialRequests;
        this._totalPrice=totalPrice;
	}
    

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter name
     * @return {String}
     */
	public get name(): String {
		return this._name;
	}

    /**
     * Getter surname
     * @return {String}
     */
	public get surname(): String {
		return this._surname;
	}

    /**
     * Getter email
     * @return {String}
     */
	public get email(): String {
		return this._email;
	}

    /**
     * Getter phone
     * @return {String}
     */
	public get phone(): String {
		return this._phone;
	}

    /**
     * Getter reservationDate
     * @return {Date}
     */
	public get reservationDate(): Date {
		return this._reservationDate;
	}

    /**
     * Getter reservationTime
     * @return {ReservationTime}
     */
	public get reservationTime(): ReservationTime {
		return this._reservationTime;
	}

    /**
     * Getter tablePreference
     * @return {TablePreference}
     */
	public get tablePreference(): TablePreference {
		return this._tablePreference;
	}

    /**
     * Getter specialRequests
     * @return {SpecialRequests[]}
     */
	public get specialRequests(): SpecialRequests[] {
		return this._specialRequests;
	}

    /**
     * Getter totalPrice
     * @return {number}
     */
	public get totalPrice(): number {
		return this._totalPrice;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter name
     * @param {String} value
     */
	public set name(value: String) {
		this._name = value;
	}

    /**
     * Setter surname
     * @param {String} value
     */
	public set surname(value: String) {
		this._surname = value;
	}

    /**
     * Setter email
     * @param {String} value
     */
	public set email(value: String) {
		this._email = value;
	}

    /**
     * Setter phone
     * @param {String} value
     */
	public set phone(value: String) {
		this._phone = value;
	}

    /**
     * Setter reservationDate
     * @param {Date} value
     */
	public set reservationDate(value: Date) {
		this._reservationDate = value;
	}

    /**
     * Setter reservationTime
     * @param {ReservationTime} value
     */
	public set reservationTime(value: ReservationTime) {
		this._reservationTime = value;
	}

    /**
     * Setter tablePreference
     * @param {TablePreference} value
     */
	public set tablePreference(value: TablePreference) {
		this._tablePreference = value;
	}

    /**
     * Setter specialRequests
     * @param {SpecialRequests[]} value
     */
	public set specialRequests(value: SpecialRequests[]) {
		this._specialRequests = value;
	}

    /**
     * Setter totalPrice
     * @param {number} value
     */
	public set totalPrice(value: number) {
		this._totalPrice = value;
	}

    
}